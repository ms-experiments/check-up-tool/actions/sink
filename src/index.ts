import { stan } from './nats';
import { getConnection } from './mongo';
import { Message } from 'node-nats-streaming';

stan.on('connect', async () => {
  const client = await getConnection();
  const db = await client.db('actions');

  stan
    .subscribe(
      'DISEASE_CREATED',
      stan.subscriptionOptions().setDeliverAllAvailable()
    )
    .on('message', async (msg: Message) => {
      const disease: any = JSON.parse(msg.getData() as string);
      await db.collection('diseases').insertOne(disease);
    });

  stan
    .subscribe(
      'ACTION_CREATED',
      stan.subscriptionOptions().setDeliverAllAvailable()
    )
    .on('message', async (msg: Message) => {
      const action: any = JSON.parse(msg.getData() as string);
      await db.collection('actions').insertOne(action);
    });
});
